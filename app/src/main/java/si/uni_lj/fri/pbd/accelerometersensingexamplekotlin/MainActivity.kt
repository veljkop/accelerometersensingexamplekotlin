package si.uni_lj.fri.pbd.accelerometersensingexamplekotlin

import android.annotation.SuppressLint
import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import si.uni_lj.fri.pbd.accelerometersensingexamplekotlin.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), SensorEventListener {

    companion object {
        private const val TAG = "MainActivity"
    }

    private lateinit var binding: ActivityMainBinding
    private lateinit var sensorManager: SensorManager
    private lateinit var sensor: Sensor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        // TODO: instantiate sensor manager
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        // TODO: if sensor present, set sensor field
        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        } else {
            Log.d(TAG, "No sensor!")
        }

    }

    override fun onResume() {
        super.onResume()

        // TODO: register sensor listener
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)

    }

    override fun onPause() {
        super.onPause()

        // TODO: unregister sensor listener
        sensorManager.unregisterListener(this)

    }

    override fun onSensorChanged(event: SensorEvent?) {
        event?:return
        val x = event.values[0]
        val y = event.values[1]
        val z = event.values[2]

        binding.tvX.text = "X: $x"
        binding.tvY.text = "Y: $y"
        binding.tvZ.text = "Z: $z"
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }

}